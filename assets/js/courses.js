//retrieve the user info for isAdmin/ retrieve info ng user kung admin or not
let adminUser = localStorage.getItem("isAdmin");

console.log(adminUser);

// will contain the html for the different buttons per user
let cardFooter;

fetch('https://cryptic-sierra-20304.herokuapp.com/api/courses')
.then(res => res.json())
.then(data => {

	console.log(data);

	// Variable to store the card/ message to show if there's no courses
	let courseData;

	if(data.length < 1) {// meaning 1 kase wala tayo ni retrieve
		courseData = "No courses available"
	} else {

		courseData = data.map(course => { //para macapture yung individual course
						// everytime we map its creating a new card storing info kada course na nkikita sa data base
			console.log(course._id);

			// logic for rendering different buttons based on the user
			if(adminUser === "false" || !adminUser){
				// how to pass data via url
				// passing data using url add ? then add paramatername(courseId)
				// relative_path? parameter_name=value&param_name2=value2
				// course.html?courseId=${course._id}
				cardFooter =
				`
					<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block editButton"> Select Course </a>
				`
			} else {
				cardFooter = 
					`
						<a href="./editCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block editButton"> Edit </a>
	                	 <a href="./deleteCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-danger text-white btn-block dangerButton"> Disable Course </a>
					`
			}

			return (
				`
					<div class="col-md-6 my-3">
                        <div class='card'>
                            <div class='card-body'>
                                <h5 class='card-title'>${course.name}</h5>
                                <p class='card-text text-left'>
                                    ${course.description}
                                </p>
                                <p class='card-text text-right'>
                                   ₱ ${course.price}
                                </p>

                            </div>
                            <div class='card-footer'>
                                ${cardFooter}
                            </div>
                        </div>
                    </div>
				`
			)
			
		}).join("")

		// courseData = [{course}, {course2}, ...]

		let coursesContainer = document.querySelector("#coursesContainer");
		coursesContainer.innerHTML = courseData;

	}

})

let modalButton = document.querySelector('#adminButton');

if(adminUser == "false" || !adminUser) {
	modalButton.innerHTML = null;
} else {
	modalButton.innerHTML = 
		`
			<div class="col-md-2 offset-md-10">
						<a href="./addCourse.html" class="btn btn-block btn-primary"> Add Course </a>
					</div>
		`
}