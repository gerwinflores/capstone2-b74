// grabbing course id from the URL/accessing whole window may access na courseID
console.log(window.location.search);
// instantiate a URLSearchParams object so we can execute method to access the parameters
// allow us to access some method(get/has) grab the courseId and its value
let params = new URLSearchParams(window.location.search);

console.log(params);
console.log(params.has('courseId'));//kung meron courseId(print as true)
console.log(params.get('courseId')); 

//grab details
let courseId = params.get('courseId');

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");

let token = localStorage.getItem('token');
console.log(userToken);

//request
fetch(`https://secret-beyond-50447.herokuapp.com/api/courses/${courseId}`)
.then(res=> res.json())
.then(data => {

	console.log(data); // lalabas yung object data for individual course

	courseName.innerHTML = data.name;
	courseDesc.innerHTML = data.description;
	coursePrice.innerHTML = data.price;
	enrollContainer.innerHTML = //added enroll button
		`
			<button id="enrollButton" class="btn btn-block btn-primary"> Enroll </button>
		`

	document.querySelector("#enrollButton").addEventListener("click", () => {

		fetch('https://cryptic-sierra-20304.herokuapp.com/api/users/enroll', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`//kukunin from localstorage
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true){

				alert("Thank you for enrolling! See you in class!");
				window.location.replace("courses.html")

			} else {
				alert("something went wrong")
			}

		})
	})

})

