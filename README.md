Tech Used:
HTML, CSS, JS - CLIENT
MongoDB, ExpressJS, NodeJS - API

API Repo: git@gitlab.com:zuitt-coding-bootcamp-curricula/courses/wdc028/wdc028-35.git

Admin Credentials
email: admin@email.com
password: Admin123

Features:
User
    - Register
    - Login
    - Enroll

Courses
    - Create a Course
    - Retrieve all Courses
    - Update a Course
    - Delete a Course

Folder Structure:
    - Root Folder
        - index.html (landing page)
        - template.html (basic bootstrap html skeleton)
        - assets folder
        - pages folder
            - pages of the webapp
        - assets Folder
            - css
            - images
            - js
                - each page has its own external js file
                - script.js file is linked to ALL html files for a dynamic navbar layout






